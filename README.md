# Thumbnails app

## Goal

This exercise is meant to demonstrate your ability to:

* Access a public API and successfully retrieve data from it;
* Display that data on a page;
* Update the UI of a page without refreshing;
* Build a polished user experience you'd be proud to ship; and
* Do all of the above using only native JavaScript (no libraries such as jQuery, although CSS preprocessors are fine).

## Running this

* `npm install`
* `npm start`
* open browser on http://localhost:3000