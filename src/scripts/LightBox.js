'use strict';

(function() {
  // Bootstrap alike Modal in Vanilla JS
  // http://getbootstrap.com/javascript/#modals
  window.LightBox = function() {

    this.closeButton = null;
    this.modal = null;
    this.backdrop = null;

    this.transitionEnd = whichTransitionEvent();

    var defaults = {
      autoOpen: false,
      closeButton: true,
      content: '',
      header: 'LightBox',
      footer: '',
      maxWidth: 600,
      minWidth: 280,
      backdrop: true
    };

    if (arguments[0] && typeof arguments[0] === 'object') {
      this.options = Object.assign(defaults, arguments[0]);
    }

    if (this.options.autoOpen === true) {
      this.open();
    }

    Build.call(this);
    SetEvents.call(this);
  };

  window.LightBox.prototype.close = function() {
    var that = this;

    document.body.classList.remove('modal-open');

    this.modal.classList.remove('in');
    this.backdrop.classList.remove('in');

    this.modal.addEventListener(this.transitionEnd, function(event) {
      if (event.target === that.modal) {
        that.modal.style.display = 'none';
      }
    });

    this.backdrop.addEventListener(this.transitionEnd, function(event) {
      if (that.backdrop.parentNode && event.target === that.backdrop) {
        that.backdrop.style.display = 'none';
      }
    });
  };

  window.LightBox.prototype.open = function() {
    document.body.classList.add('modal-open');

    /*jshint -W030 */
    window.getComputedStyle(this.modal).height;

    this.modal.classList.add('in');
    this.backdrop.classList.add('in');

    this.modal.style.display = 'block';
    this.backdrop.style.display = 'block';
  };

  function Build () {

    var contentHolder, headerHolder,
      footerHolder, docFrag,
      modalDialog, modalContent;

    // Create a DocumentFragment to build with
    docFrag = document.createDocumentFragment();

    // Create modal element
    this.modal = document.createElement('div');
    this.modal.className = 'modal fade in';

    modalDialog = document.createElement('div');
    modalDialog.className = 'modal-dialog modal-lg';

    modalContent = document.createElement('div');
    modalContent.className = 'modal-content';

    contentHolder = document.createElement('div');
    contentHolder.className = 'modal-body';

    if (typeof this.options.content === 'string') {
      contentHolder.innerHTML = this.options.content;
    } else {
      this.options.content.parentNode.removeChild(this.options.content);
      contentHolder.appendChild(this.options.content);
    }

    headerHolder = document.createElement('div');
    headerHolder.className = 'modal-header';

    if (typeof this.options.header === 'string') {
      headerHolder.innerHTML = this.options.header;
    } else {
      this.options.header.parentNode.removeChild(this.options.header);
      headerHolder.appendChild(this.options.header);
    }

    if (this.options.closeButton === true) {
      this.closeButton = document.createElement('button');
      this.closeButton.classList.add('close');
      this.closeButton.innerHTML = '<span>&times;</span>';

      headerHolder.insertBefore(this.closeButton, headerHolder.childNodes[0]);
    }

    footerHolder = document.createElement('div');
    footerHolder.className = 'modal-footer';

    if (typeof this.options.footer === 'string') {
      footerHolder.innerHTML = this.options.footer;
    } else {
      this.options.footer.parentNode.removeChild(this.options.footer);
      footerHolder.appendChild(this.options.footer);
    }

    modalContent.appendChild(headerHolder);
    modalContent.appendChild(contentHolder);
    modalContent.appendChild(footerHolder);

    modalDialog.appendChild(modalContent);

    this.modal.appendChild(modalDialog);

    docFrag.appendChild(this.modal);

    if (this.options.backdrop === true) {
      this.backdrop = document.createElement('div');
      this.backdrop.className = 'modal-backdrop fade';
      docFrag.appendChild(this.backdrop);
    }

    this.modal.style.display = 'none';
    this.backdrop.style.display = 'none';

    document.body.appendChild(docFrag);
  }

  function SetEvents () {
    if (this.closeButton) {
      this.closeButton.addEventListener('click', this.close.bind(this));
    }
  }

  // https://davidwalsh.name/css-animation-callback
  function whichTransitionEvent(){
    var t;
    var el = document.createElement('fakeelement');
    var transitions = {
      'transition':'transitionend',
      'OTransition':'oTransitionEnd',
      'MozTransition':'transitionend',
      'WebkitTransition':'webkitTransitionEnd'
    };

    for(t in transitions){
        if( el.style[t] !== undefined ){
            return transitions[t];
        }
    }
  }
})();