// LightBox configuration and set up
var content = document.getElementById('content');
var header = document.getElementById('header');
var footer = document.getElementById('footer');
var bigImage = document.getElementById('bigImage');
var extraClose = document.getElementById('extraClose');
var previous = document.getElementById('previous');
var next = document.getElementById('next');

var myLightBox = new LightBox({
  content: content,
  header: header,
  footer: footer
});

extraClose.addEventListener('click', function () {
    myLightBox.close();
});

next.addEventListener('click', function () {
    var newSrc = thumbnails.nextSrc(bigImage.src);
    bigImage.src = newSrc;
});

previous.addEventListener('click', function () {
    var newSrc = thumbnails.prevSrc(bigImage.src);
    bigImage.src = newSrc;
});

function openLightBox (src) {
    bigImage.src = src;
    myLightBox.open();
}

// Thumbnails configurations and set up
var thumbnails = new Thumbnails(document.getElementById('Thumbnails'), hideLoader, openLightBox);
var loader = document.getElementById('loader');

// Infinite scroll set up usint thumbnails functionality
var infiniteScrollOptions = {
    distance: 10,
    callback: function(done) {
        showLoader();
        thumbnails.fill(2, function () {
            hideLoader();
            done();
        });
    }
};

infiniteScroll(infiniteScrollOptions);

// UX for loading advice
function hideLoader () {
    loader.style.display = 'none';
}

function showLoader () {
    loader.style.display = 'block';
}