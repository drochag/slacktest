'use strict';

(function() {
  window.Thumbnails = function(target, finishFirstLoad, onClick) {
    var that = this;

    this.target = target;
    this.onClick = onClick;
    this.usedImages = [];
    this.fill(6, finishFirstLoad);
  };

  window.Thumbnails.prototype.nextSrc = function (current) {
    var nextIdx = this.usedImages.indexOf(current) + 1;
    return this.usedImages[nextIdx] ? this.usedImages[nextIdx] : this.usedImages[0];
  };

  window.Thumbnails.prototype.prevSrc = function (current) {
    var nextIdx = this.usedImages.indexOf(current) - 1;
    return this.usedImages[nextIdx] ? this.usedImages[nextIdx] : this.usedImages[this.usedImages.length - 1];
  };

  window.Thumbnails.prototype.fill = function(quantity, done) {
    if (!quantity) {
      quantity = 2;
    }

    var i = 0, finished = 0;

    for (i = 0; i < quantity; i++) {
      AppendImage.call(this);
    }

    function AppendImage () {
      FetchImage.call(this, function (src) {
        if (!src) {
          checkDone();
          return;
        }

        var img = document.createElement('img');

        img.classList.add('Thumbnails-image');
        img.src = src;
        img.onclick = this.onClick.bind(null, src);

        this.target.appendChild(img);

        checkDone();
      });
    }

    function FetchImage (cb) {
      var that = this;
      var xmlHttp = new XMLHttpRequest();

      xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
          if (!that.usedImages.length) {
            that.usedImages.push(xmlHttp.responseURL);
          } else if (that.usedImages.indexOf(xmlHttp.responseURL) >= 0) {
            return FetchImage.call(that, cb);
          } else {
            that.usedImages.push(xmlHttp.responseURL);
          }

          cb.call(that, xmlHttp.responseURL);
        } else if (xmlHttp.readyState === 4) {
          cb.call(that);
        }
      };

      xmlHttp.open('GET', 'https://source.unsplash.com/random');
      xmlHttp.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8');
      xmlHttp.send(null);
    }

    function checkDone () {
      finished++;
      if (done && finished === quantity) {
        done();
      }
    }
  };
})();